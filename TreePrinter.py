import AST


def addToClass(cls):
    def decorator(func):
        setattr(cls, func.__name__, func)
        return func

    return decorator


class TreePrinter:
    @addToClass(AST.Node)
    def printTree(self):
        raise Exception("printTree not defined in class " + self.__class__.__name__)

    @addToClass(AST.BinExpr)
    def printTree(self, depth=0):
        return "| " * depth + self.op + "\n" + self.left.printTree(depth + 1) + self.right.printTree(depth + 1)

    @addToClass(AST.CodeBlocks)
    def printTree(self):
        # print every code block
        return "".join(x.printTree() for x in self.code_blocks)

    @addToClass(AST.Declarations)
    def printTree(self, depth=0):
        return "".join(x.printTree(depth) for x in self.declarations)

    @addToClass(AST.Declaration)
    def printTree(self, depth=0):
        return "| " * depth + "DECL\n" + self.inits.printTree(depth + 1)

    @addToClass(AST.Inits)
    def printTree(self, depth=0):
        return "".join(x.printTree(depth) for x in self.inits)

    @addToClass(AST.Init)
    def printTree(self, depth=0):
        return "| " * depth + "=\n" + "| " * (depth + 1) + str(self.id) + "\n" + self.expression.printTree(depth + 1)

    @addToClass(AST.Instructions)
    def printTree(self, depth=0):
        return "".join(x.printTree(depth) for x in self.instructions)

    @addToClass(AST.PrintInstruction)
    def printTree(self, depth=0):
        return "| " * depth + "PRINT\n" + self.expr_list.printTree(depth + 1)

    @addToClass(AST.LabeledInstruction)
    def printTree(self, depth=0):
        return "| " * depth + "LABELED\n" + "| " * (depth + 1) + str(self.id) + "\n" + \
               self.instr.printTree(depth + 1)

    @addToClass(AST.Assignment)
    def printTree(self, depth=0):
        return "| " * depth + "=" + "\n" + "| " * (depth + 1) + str(self.id) + "\n" + \
               self.expression.printTree(depth + 1)

    @addToClass(AST.ChoiceInstr)
    def printTree(self, depth=0):
        result = "| " * depth + "IF" + "\n" + self.condition.printTree(depth + 1) + \
                 self.instruction.printTree(depth + 1)
        if self.else_instruction:
            result += "| " * depth + "ELSE" + "\n" + self.else_instruction.printTree(depth + 1)
        return result

    @addToClass(AST.WhileInstr)
    def printTree(self, depth=0):
        return "| " * depth + "WHILE" + "\n" + self.condition.printTree(depth + 1) + \
               self.instruction.printTree(depth + 1)

    @addToClass(AST.RepeatInstr)
    def printTree(self, depth=0):
        return "| " * depth + "REPEAT" + "\n" + self.instructions.printTree(depth + 1) + "\n" + \
               "| " * depth + "UNTIL" + "\n" + self.instruction.printTree(depth + 1)

    @addToClass(AST.ReturnInstr)
    def printTree(self, depth=0):
        return "| " * depth + "RETURN" + "\n" + self.expression.printTree(depth + 1)

    @addToClass(AST.Label)
    def printTree(self, depth=0):
        return "| " * depth + self.label + "\n"

    @addToClass(AST.Compound)
    def printTree(self, depth=0):
        if self.declarations:
            return self.declarations.printTree(depth) + self.instructions_opt.printTree(depth + 1)
        else:
            return ""

    @addToClass(AST.FunctionInvokation)
    def printTree(self, depth=0):
        return "| " * depth + "FUNCALL" + "\n" + "| " * (depth + 1) + self.id + "\n" + self.args.printTree(depth + 1)

    @addToClass(AST.Arg)
    def printTree(self, depth=0):
        return "| " * depth + self.id + '\n'

    @addToClass(AST.ExpressionList)
    def printTree(self, depth=0):
        res = ""
        for expr in self.expressions:
            res += expr.printTree(depth)
        return res

    @addToClass(AST.FunDefs)
    def printTree(self, depth=0):
        res = ""
        for fundef in self.fundefs:
            res += fundef.printTree(depth)
        return res

    @addToClass(AST.Args)
    def printTree(self, depth=0):
        res = ""
        for arg in self.args:
            res += "| " * depth + "ARG " + arg.id + "\n"
        return res

    @addToClass(AST.FunDef)
    def printTree(self, depth=0):
        fundef = "| " * depth + "FUNDEF" + "\n" + "| " * (depth + 1) + self.id + "\n" + "| " * (
            depth + 1) + "RET " + self.type + "\n"
        fundef += self.args.printTree(depth + 1)
        fundef += self.instructions.printTree(depth + 1)
        return fundef

    @addToClass(AST.Const)
    def printTree(self, depth=0):
        return "| " * depth + str(self.value) + "\n"
