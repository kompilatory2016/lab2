class Node(object):
    def __str__(self):
        return self.printTree()


class BinExpr(Node):
    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right


class FunctionInvokation(Node):
    def __init__(self, id, args):
        self.id = id
        self.args = args


class ExpressionList(Node):
    def __init__(self):
        self.expressions = list()


class Const(Node):
    def __init__(self, value):
        self.value = value


class Integer(Const):
    pass
    # ...


class Float(Const):
    pass
    # ...


class String(Const):
    pass
    # ...


class Variable(Node):
    pass
    # ...


class Compound(Node):
    def __init__(self, declarations, instructions_opt):
        self.declarations = declarations
        self.instructions_opt = instructions_opt


class CodeBlocks(Node):
    def __init__(self):
        self.code_blocks = list()


class ChoiceInstr(Node):
    def __init__(self, condition, instruction, else_instruction=None):
        self.condition = condition
        self.instruction = instruction
        self.else_instruction = else_instruction


class WhileInstr(Node):
    def __init__(self, condition, instruction):
        self.condition = condition
        self.instruction = instruction


class RepeatInstr(Node):
    def __init__(self, instructions, condition):
        self.instructions = instructions
        self.condition = condition


class Declarations(Node):
    def __init__(self):
        self.declarations = list()


class Declaration(Node):
    def __init__(self, type, inits):
        self.type = type
        self.inits = inits


class Inits(Node):
    def __init__(self):
        self.inits = list()


class Init(Node):
    def __init__(self, id, expression):
        self.id = id
        self.expression = expression


class FunDefs(Node):
    def __init__(self):
        self.fundefs = list()


class FunDef(Node):
    def __init__(self, type, id, args, instructions):
        self.type = type
        self.id = id
        self.args = args
        self.instructions = instructions


class Instructions(Node):
    def __init__(self):
        self.instructions = list()


class PrintInstruction(Node):
    def __init__(self, expr_list):
        self.expr_list = expr_list


class LabeledInstruction(Node):
    def __init__(self, id, instruction):
        self.id = id
        self.instruction = instruction


class ReturnInstr(Node):
    def __init__(self, expression):
        self.expression = expression


class Label(Node):
    # continue break etc.
    def __init__(self, label):
        self.label = label


class Args(Node):
    def __init__(self):
        self.args = list()


class Arg(Node):
    def __init__(self, type, id):
        self.type = type
        self.id = id


class Assignment(Node):
    def __init__(self, id, expression):
        self.id = id
        self.expression = expression
